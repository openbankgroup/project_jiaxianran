<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script type="text/javascript">
	var issubmit=1;
	var errmsg="<%=session.getAttribute("errmsg") %>";
	if(errmsg!="" && errmsg!=undefined){
		issubmit=0;
		alert(errmsg);
	}
	function autoSubmit (){ 
		if(issubmit==1){
			form.submit();
		}else{
			window.location.href="index.html";
		}
	}
</script>
</head>
<body onload="autoSubmit();">
<form id="form" name="form" method="post" action="<%=session.getAttribute("redirectURL") %>">
	<input type="hidden" name="encryptBody" value="<%=session.getAttribute("encryptBody") %>" />
	<input type="hidden" name="sign" value="<%=session.getAttribute("sign") %>" />
</form>
</body>
</html>