package com.citicbank;

import java.io.IOException;
import java.security.PublicKey;
import java.security.cert.X509Certificate;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.citicbank.cbframework.common.exception.CBInvalidParameterException;
import com.citicbank.cbframework.common.util.CBBase64;
import com.lsy.baselib.crypto.protocol.PKCS7Signature;
import com.lsy.baselib.crypto.util.Base64;
import com.lsy.baselib.crypto.util.CryptUtil;
import com.lsy.baselib.util.BytesUtil;
import com.lsy.baselib.xml.XmlDocument;
import com.lsy.baselib.xml.element.XmlEBStream;
import com.lsy.baselib.xml.element.XmlField;

/**
 * 接收异步通知回调
 * 
 * @author yxyc-liuchunshu
 * 
 */
@WebServlet(urlPatterns={"/callback"})
public class Callback extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.setCharacterEncoding("UTF-8");
		resp.setCharacterEncoding("UTF-8");
		StringBuffer sbRequest = new StringBuffer(30000);
		try {
			ServletInputStream in = req.getInputStream();
			// 从输入流中读取数据并保存在缓冲区里
			byte[] baBuf = new byte[30000];
			int length = 0;
			while (-1 != (length = in.read(baBuf))) {
				// sbRequest.append(new String(baBuf, 0, length,
				// ConstantCotb.UTF8_ENCODING));
				sbRequest.append(BytesUtil.getStringFromBytes(baBuf, 0, length));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		String request = sbRequest.toString();
		int requestLen = req.getContentLength();
		if (-1 == requestLen) {
			requestLen = 0;
		}
		if (request.length() > requestLen) {
			request = request.substring(0, requestLen);
		}
		// 由于BytesUtil.getStringFromBytes中使用的ISO8859-1接收，所以这里需要转化一下。
		request = new String(request.getBytes("ISO8859-1"), "UTF-8");

		// request中只有一个字段
		XmlEBStream xmlstream = XmlDocument.parseXmlEBStream(request);
		String signresmsg = xmlstream.getFieldValue("SIGNRESMSG");
		try {
			// BASE64解码后得到明文数据
			signresmsg = new String(CBBase64.decode(signresmsg.getBytes("UTF-8")), "UTF-8");
		} catch (CBInvalidParameterException e1) {
			e1.printStackTrace();
		}
		System.out.println("异步通知结果："+signresmsg);
		// 将signresmsg中的明文按ASCII排序后，将除SIGN字段外的其他字段的VALUE拼接成一个字符串作为验签的明文
		xmlstream = XmlDocument.parseXmlEBStream(signresmsg);
		@SuppressWarnings("unchecked")
		List<XmlField> list1 = xmlstream.getAllField();
		Collections.sort(list1, new Comparator<XmlField>() {
			@Override
			public int compare(XmlField o1, XmlField o2) {
				return o1.getName().compareTo(o2.getName());
			}
		});
		StringBuffer sbf = new StringBuffer();
		for (int j = 0; j < list1.size(); j++) {
			XmlField field = (XmlField) list1.get(j);
			if ("sign".equalsIgnoreCase(field.getName())) {
				continue;
			}
			sbf.append(field.getValue());
		}
		// 验签的明文
		String verifyData = sbf.toString();
		String signData = xmlstream.getFieldValue("SIGN");
		//验签证书
		String publicKey="MIIDITCCAgmgAwIBAgIBMDANBgkqhkiG9w0BAQUFADArMQswCQYDVQQGEwJDTjENMAsGA1UECwwEUFROUjENMAsGA1UEAwwEdGVzdDAeFw0xNzA2MDEwMjMzNDZaFw0yNzA1MzAwMjMzNDZaMCsxCzAJBgNVBAYTAkNOMQ0wCwYDVQQLDARQVE5SMQ0wCwYDVQQDDAR0ZXN0MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAto3/T7lVxrlHOo1XcpwniHmCLxnnMU+9TMglpiy+Z6JeDzgHj1XX1dyf5fOXT/lV3F8A226rXD6WQS7M9vv4mcBlf0nbCCz/m+2wJPMhvpDetewaKn0tGnXudqM/vEWKzk4ePs+12elePsDr58I+ffXOTzZALrutY0rn4tsHkqBP7jWYY7UpERR+aFptbIMzTR3iMJY4LDbNu13DVMpbKKWLMheB2i4FJr8kZhC0Fnk3sltkN0XNOi4tRhzqZqIttXWWhTEnj3JKd+ZA3H2BovujpkNayPiyFFl+Jhd6wfnPLgBSCq4CjNm3OYIuIFb6zMEyWRRFbYSBa789hWSCPQIDAQABo1AwTjAdBgNVHQ4EFgQU9FIGuf/328a525OrV+7B4owIRPcwHwYDVR0jBBgwFoAU9FIGuf/328a525OrV+7B4owIRPcwDAYDVR0TBAUwAwEB/zANBgkqhkiG9w0BAQUFAAOCAQEAs+iRoOBndGaSdH8s3KZyy8uzNQKRF0wazQkKdFDuvzxrylbjZUzSS6cpkwuUgBAulLxpNrWzqyejRQ2yz6iiArtixBFiGhv9B380g1szKnCoVExFumdudCuQPMaUScyAr8Z9d17/EhQLTn8IAy1Sa1iLzOTONnMessZpf/PaguGZLwpkaU55syL4P3PAhLveHp/t7q6bTY/iAQrcc0Oqbngpk4vX8+9BqUrjIRs4cIjhU8K8+yHYPqogNC9FD2NL8JkMzpa/ekKR0knbdCaAB1rcuenFbGQAnS/JscndJv/xqW2z/psqb5XS9ivTRD5f67G0Bluo5ZdUrzrgkrjSrQ==";

		try {
			boolean verifySignResult=verifySign(verifyData.getBytes("UTF-8"),signData,publicKey);
			System.out.println("异步通知验签结果："+verifySignResult);
		} catch (Exception e) {
			e.printStackTrace();
		}
		String respStr="<stream><return_code>SUCCESS</return_code></stream>";//接收异步通知成功-SUCCESS；接收异步通知失败-FAIL
		resp.getOutputStream().write(respStr.getBytes("UTF-8"));
	}

	private Boolean verifySign(byte[] msg, String sign, String sender_cer_file_nm) throws Exception {
		byte[] base64EncodedSenderCert = null;
		try {
			base64EncodedSenderCert = sender_cer_file_nm.getBytes();
		} catch (Exception e) {
			e.printStackTrace();
		}
		X509Certificate signerCertificate = null;
		try {
			signerCertificate = CryptUtil.generateX509Certificate(Base64.decode(base64EncodedSenderCert));
		} catch (Exception e) {
			e.printStackTrace();
		}
		PublicKey senderPubKey = signerCertificate.getPublicKey();
		return PKCS7Signature.verifyDetachedSignature(msg, Base64.decode(sign.getBytes()), senderPubKey);
	}
}
