package com.citicbank;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.Key;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAKey;
import java.security.interfaces.RSAPrivateKey;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;
import java.util.zip.Inflater;

import javax.crypto.Cipher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.alibaba.fastjson.JSONObject;
import com.citicbank.cbframework.common.security.CBRSA;
import com.citicbank.cbframework.common.util.CBConverter;
import com.lsy.baselib.crypto.util.Base64;
import com.lsy.baselib.crypto.util.FileUtil;

@WebServlet(urlPatterns={"/h5demoServer"})
public class H5demoServer extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		req.setCharacterEncoding("UTF-8");
		resp.setCharacterEncoding("UTF-8");
		String redirectURL = req.getParameter("redirectURL");
		HttpSession session = req.getSession();

		String paramsList = req.getParameter("paramsList");
		if (paramsList == null || "".equals(paramsList)) {
			session.setAttribute("errmsg", "参数列表为空");
		} else {
			session.setAttribute("errmsg", "");
		}

		String[] paramArr = paramsList.split(",");

		JSONObject json = new JSONObject();
		for (int i = 0; i < paramArr.length; i++) {
			String paraStr = paramArr[i];
			String value = req.getParameter(paraStr);
			json.put(paraStr, value);
		}

		String OPENTRANSCODE = req.getParameter("OPENTRANSCODE");
		json.put("OPENTRANSCODE", OPENTRANSCODE);
		String OPENMERCODE = req.getParameter("OPENMERCODE");
		json.put("OPENMERCODE", OPENMERCODE);
		String OPENBUSITYPE = req.getParameter("OPENBUSITYPE");
		json.put("OPENBUSITYPE", OPENBUSITYPE);
		String OPENMERNAME = req.getParameter("OPENMERNAME");
		json.put("OPENMERNAME", OPENMERNAME);
		json.put("OPENVER", "1.0.0");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		json.put("OPENLAUNCHDATE", sdf.format(new Date()));
		SimpleDateFormat sdf1 = new SimpleDateFormat("HHmmss");
		json.put("OPENLAUNCHTIME", sdf1.format(new Date()));
		SimpleDateFormat sdf2 = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		String uuid = UUID.randomUUID().toString().replace("-", "");
		json.put("OPENMERFLOWID", uuid + sdf2.format(new Date()));

		String jsonstr = json.toJSONString();
		String sign = signData(jsonstr);
		String encryptBody = null;
		try {
			encryptBody = encryptBusiness(jsonstr);
		} catch (Exception e) {
			e.printStackTrace();
		}

		JSONObject busijson = new JSONObject();
		busijson.put("encryptBody", encryptBody);
		busijson.put("sign", sign);
		
		JSONObject resultMsg = null;
		try {
			resultMsg = sendBusinesstoServer(redirectURL,busijson.toJSONString(),OPENTRANSCODE);
		} catch (Exception e) {
			e.printStackTrace();
		}
		resp.getWriter().write(resultMsg.toJSONString());
	}
	
	private JSONObject sendBusinesstoServer(String urlstr,String business,String openTransCode) throws Exception{
		URL url = new URL(urlstr);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod("POST");
		conn.setConnectTimeout(5000);
		conn.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
		conn.setRequestProperty("connection", "keep-alive");
		conn.setUseCaches(false);
		conn.setInstanceFollowRedirects(true);
		conn.setDoInput(true);
		conn.setDoOutput(true);
		conn.connect();
		OutputStream os = conn.getOutputStream();
		os.write(business.getBytes("UTF-8"));
		os.flush();
		os.close();
		InputStream is = conn.getInputStream();
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		String respstr = null;
		StringBuilder sbr=new StringBuilder();
		while ((respstr = br.readLine()) != null) {
			sbr.append(respstr);
		}
		br.close();
		is.close();

		String responString=sbr.toString();
		System.out.println(responString);
		
		JSONObject jsonObject = JSONObject.parseObject(responString);
		//获取外联平台传输的业务报文
		String respbusiness = jsonObject.getString("encryptBody");
		String signStr = jsonObject.getString("sign");
		
		String[] encryptBusinessDataArr=respbusiness.split("@@");
		String responencryptKey=encryptBusinessDataArr[1];

		RSAPrivateKey privateKey = (RSAPrivateKey) getPrivateKeyForAes();
		byte[] keyByte=CBRSA.decrypt(privateKey, CBConverter.base64ToBytes(responencryptKey));
		String encryptKeystr=new String(keyByte,"UTF-8");
		System.out.println(encryptKeystr);
		byte[] decryptedBusinessDataBytes = AESUtil.decrypt(CBConverter.base64ToBytes(encryptBusinessDataArr[0]), encryptKeystr);
		String decryptedBusinessData = new String(decryptedBusinessDataBytes, "UTF-8");

		boolean issign=verifySign(decryptedBusinessData.getBytes("UTF-8"),signStr);
		System.out.println("sign is "+issign);

		//下载对账文件接口返回：{"RESPCODE":"AAAAAAA","RESPMSG":"下载成功","REMOTEFILENAME":"/jzsd/file/ZFB/BCCS_001_YCX000000100TEST1_20200113.txt","FILEMSG":"eNrt201qGzEYBuDF7HIKn2D4fvR7gJwgWfiG3ndjQuJVKaVJIE76s29WOUJIIETjErBdOWqjYLn1KzAM4vXHzFh+0FjymBaNiY4Pj455tKl1Xy5PJ7P7s6evH0f/dOtOHmY/P9yef5v+mM/fpSAza7p/WgxmO4Uk3X5Wihxs7Jywd5F8queFbRgis7uzz98vTifpMJ3zp5v7TSfeSU+02kO/9eQzK32ZdxUv5O9umWr0VFGg+gyqCxQ+CZ9GBfGvw8XXa3moyEvq6nF6vXFEvByYbmtXtU+tOxjDvt2wj0lCGuZ19in1MRbsy2ca2MewD/bBPtiXKmgwtfaxib2T1xXLZxrYZ2Af7IN9sC9VMIFq7bPraGUUy2ca2BdgH+yDfbBPKdj02qPf+xzmfbAP9sG+hX3R7JV9FvbBPtgH+5QiCVOlfcksLtmXzzSwz8E+2Af7YN9gn5fqtY5ei/O+fKaBfR72wT7YB/uULQXVJftEWcj9v/O+iD0usA/2wb7BPnYr9r3tmZeL8758Ztv2GSLsbYZ9sA/2DfZpFFu71kFrauXWOrKZBvYJ7IN9sA/2JftMoq7SPvcH9rldsQ97XGAf7IN9qYJTY2vXOsT78rwvm9m+fQz7YB/sg32DfdYZV2uf66OMCvZlMw3sw/86YB/sg32pQjBcbZ/tXXGtI5/Zvn2iLemCfbAP9u2IfUIcVfZonRf7+2BfW/ueAe/Gc8U=","RETCODE":"AAAAAAA","OPENMERCODE":"CITICIFOP9999999"}
		if("IFOPF013".equals(openTransCode)){
			JSONObject respjson=JSONObject.parseObject(decryptedBusinessData);
			String filemsg=(String) respjson.get("FILEMSG");
			byte[] filebytes=Base64.decode(filemsg.getBytes("GBK"));
			byte[] uncomfile=uncompress(filebytes);
			String uncomfilemsg=new String(uncomfile,"GBK");
			decryptedBusinessData=uncomfilemsg;
			System.out.println("下载的对账文件为：");
			System.out.println(uncomfilemsg);
		}

		JSONObject resultMsg=new JSONObject();
		resultMsg.put("decryptedBusinessData", decryptedBusinessData);
		resultMsg.put("issign", issign);
		return resultMsg;
	}
	
	public static Boolean verifySign(byte[] msg, String signData) throws Exception {
        String publicKey = "MIIDITCCAgmgAwIBAgIBMDANBgkqhkiG9w0BAQUFADArMQswCQYDVQQGEwJDTjENMAsGA1UECwwEUFROUjENMAsGA1UEAwwEdGVzdDAeFw0xNzA2MDEwMjMzNDZaFw0yNzA1MzAwMjMzNDZaMCsxCzAJBgNVBAYTAkNOMQ0wCwYDVQQLDARQVE5SMQ0wCwYDVQQDDAR0ZXN0MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAto3/T7lVxrlHOo1XcpwniHmCLxnnMU+9TMglpiy+Z6JeDzgHj1XX1dyf5fOXT/lV3F8A226rXD6WQS7M9vv4mcBlf0nbCCz/m+2wJPMhvpDetewaKn0tGnXudqM/vEWKzk4ePs+12elePsDr58I+ffXOTzZALrutY0rn4tsHkqBP7jWYY7UpERR+aFptbIMzTR3iMJY4LDbNu13DVMpbKKWLMheB2i4FJr8kZhC0Fnk3sltkN0XNOi4tRhzqZqIttXWWhTEnj3JKd+ZA3H2BovujpkNayPiyFFl+Jhd6wfnPLgBSCq4CjNm3OYIuIFb6zMEyWRRFbYSBa789hWSCPQIDAQABo1AwTjAdBgNVHQ4EFgQU9FIGuf/328a525OrV+7B4owIRPcwHwYDVR0jBBgwFoAU9FIGuf/328a525OrV+7B4owIRPcwDAYDVR0TBAUwAwEB/zANBgkqhkiG9w0BAQUFAAOCAQEAs+iRoOBndGaSdH8s3KZyy8uzNQKRF0wazQkKdFDuvzxrylbjZUzSS6cpkwuUgBAulLxpNrWzqyejRQ2yz6iiArtixBFiGhv9B380g1szKnCoVExFumdudCuQPMaUScyAr8Z9d17/EhQLTn8IAy1Sa1iLzOTONnMessZpf/PaguGZLwpkaU55syL4P3PAhLveHp/t7q6bTY/iAQrcc0Oqbngpk4vX8+9BqUrjIRs4cIjhU8K8+yHYPqogNC9FD2NL8JkMzpa/ekKR0knbdCaAB1rcuenFbGQAnS/JscndJv/xqW2z/psqb5XS9ivTRD5f67G0Bluo5ZdUrzrgkrjSrQ==";
        //验证签名
        byte[] base64EncodedSenderCert = publicKey.getBytes("UTF-8");

        X509Certificate signerCertificate = CryptUtil.generateX509Certificate(Base64
                .decode(base64EncodedSenderCert));
        PublicKey signpublicKey = signerCertificate.getPublicKey();
        Signature signature = Signature.getInstance("SHA1WithRSA");
        signature.initVerify(signpublicKey);
        signature.update(msg);
        return signature.verify(Base64.decode(signData.getBytes("UTF-8")));
	}

	private String encryptBusiness(String business) throws Exception {
		String encryptKey = AESUtil.getRandomAESKey();
		byte[] encryptBusinessDataByte = AESUtil.encrypt(business, encryptKey);

		String encryptBusiness = new String(
				Base64.encode(encryptBusinessDataByte), "UTF-8");

		String publicKey = "MIIDQTCCAimgAwIBAgIBMDANBgkqhkiG9w0BAQUFADA6MQswCQYDVQQGEwJDTjENMAsGA1UECwwEQ05DQjEcMBoGA1UEAwwTYjJjLmJhbmsuZWNpdGljLmNvbTAgFw0xODEwMjIwMjU0MjZaGA8yMTE4MDkyODAyNTQyNlowOjELMAkGA1UEBhMCQ04xDTALBgNVBAsMBENOQ0IxHDAaBgNVBAMME2IyYy5iYW5rLmVjaXRpYy5jb20wggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCJe5k8J5oxkRbfrNHHbN8MEce9lkV/HWrKtpEtknRICpyT24Rx3xiCkyFfC2Zn0sYofJAcAvKElDupIIuYJHMvwWgakYHVHAbsp8XfGDToDeZdabCPSaV/tC+pbcZjnF+Zdlmy/TL9Yzxe/lSqhAnjk2wIeBQUy27CpcOKO7GXMVFE9TgogqDND+hYYzyaj+8gh73DRu2xSAq+ZTvJuW+bPvuALEOK/Ex8MO9u0oV1nn3OwkPkE/98jSs8BWBRDDGuh9OaZrUoNsF7II/e10Ad93lPf7UTEAiosO7ZiyJ+MBGy+cbkCwjpe5OOrkFr+9GNI6pjcDYul2KgI6XXdX0bAgMBAAGjUDBOMB0GA1UdDgQWBBTFTb+SBCIKxAdjuaOlferd/wKWYTAfBgNVHSMEGDAWgBTFTb+SBCIKxAdjuaOlferd/wKWYTAMBgNVHRMEBTADAQH/MA0GCSqGSIb3DQEBBQUAA4IBAQA4zYjdRonussCKJGAK+KKI1Ov8ptG9lxuUEjDFCYGG0hgyDfUxuNAIWI3GaDIxfreR/YMXPEOzEl4yORzFEu/ekh5gQvNT674x29BaA1iOKW1w5OJ7N1msJ76J66V4o1FQuFxZFO3U5nIx10eMe1VUezBIRrVO0YXMnCwA4Ckc9m1+5vEWhB78oLLSLgNhUcIwDyXyyb48TjeUO9TGCDECaadLLPsKSEiQ/fp5Nbo2VFrrx3IUMBJDxu6LiXAEZwQ8/cc8O//sT7t5ezu4joLNKGU8Sl6MSW5k2cee76g52MaA0Zt1x3G2viAMtkj5w1hJs8rsMA7+aty6tnbELWZe";
		byte[] base64EncodedSenderCert = publicKey.getBytes("UTF-8");

		X509Certificate signerCertificate = CryptUtil
				.generateX509Certificate(com.lsy.baselib.crypto.util.Base64
						.decode(base64EncodedSenderCert));
		PublicKey signpublicKey = signerCertificate.getPublicKey();

		byte[] encryptKeyByte = encrypt((RSAKey) signpublicKey,
				encryptKey.getBytes("UTF-8"));
		String encryptKeyString = new String(Base64.encode(encryptKeyByte),
				"UTF-8");

		encryptBusiness = encryptBusiness + "@@" + encryptKeyString;

		return encryptBusiness;
	}

	private byte[] encrypt(RSAKey key, byte[] data) {
		try {
			Cipher cipher = Cipher.getInstance("RSA/ECB/NoPadding", "BC");
			cipher.init(1, (Key) key);
			int step = key.getModulus().bitLength() / 8;
			int n = data.length / step;
			if (n > 0) {
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				for (int i = 0; i < n; i++) {
					baos.write(cipher.doFinal(data, i * step, step));
				}
				if ((n = data.length % step) != 0) {
					baos.write(cipher.doFinal(data, data.length - n, n));
				}
				return baos.toByteArray();
			}
			return cipher.doFinal(data);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private String signData(String data) {
		try {
			byte[] msg = data.getBytes("UTF-8");

			// 签名数据
			PrivateKey privateKey = getPrivateKey();
			Signature signature = Signature.getInstance("SHA1WithRSA");
			signature.initSign(privateKey);
			signature.update(msg);
			byte[] bytarr = signature.sign();

			String signData = new String(Base64.encode(bytarr), "UTF-8");
			signData = signData.replaceAll("\r|\n", "");
			return signData;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private PrivateKey getPrivateKey() throws Exception {
		// 私钥文件路径
		String keyfile = H5demoServer.class.getClassLoader()
				.getResource("signkey/server_sign.key").getPath();
		byte[] base64EncodedPrivatekey = FileUtil.read4file(keyfile);

		// 私钥密码文件路径
		String pwdfile = H5demoServer.class.getClassLoader()
				.getResource("signkey/server_sign.pwd").getPath();
		byte[] base64EncodedPrivatekeyPass = FileUtil.read4file(pwdfile);

		char[] keyPassword = new String(base64EncodedPrivatekeyPass, "UTF-8")
				.toCharArray();

		PrivateKey signerPrivatekey = CryptUtil.decryptPrivateKey( Base64.decode(base64EncodedPrivatekey), keyPassword);
		return signerPrivatekey;
	}
	
	private PrivateKey getPrivateKeyForAes() throws Exception {
		// 私钥文件路径
		String keyfile = H5demoServer.class.getClassLoader()
				.getResource("signkey/server_cert.key").getPath();
		byte[] base64EncodedPrivatekey = FileUtil.read4file(keyfile);

		// 私钥密码文件路径
		String pwdfile = H5demoServer.class.getClassLoader()
				.getResource("signkey/server_cert.pwd").getPath();
		byte[] base64EncodedPrivatekeyPass = FileUtil.read4file(pwdfile);

		char[] keyPassword = new String(base64EncodedPrivatekeyPass, "UTF-8")
				.toCharArray();

		PrivateKey signerPrivatekey = CryptUtil.decryptPrivateKey( Base64.decode(base64EncodedPrivatekey), keyPassword);
		return signerPrivatekey;
	}
	
	/**
     * 
     * @param inputByte
     *            待解压缩的字节数组
     * @return 解压缩后的字节数组
     * @throws IOException
     */
    public static byte[] uncompress(byte[] inputByte) throws IOException {
        int len = 0;
        Inflater infl = new Inflater();
        infl.setInput(inputByte);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        byte[] outByte = new byte[1024];
        try {
            while (!infl.finished()) {
                // 解压缩并将解压缩后的内容输出到字节输出流bos中
                len = infl.inflate(outByte);
                if (len == 0) {
                    break;
                }
                bos.write(outByte, 0, len);
            }
            infl.end();
        } catch (Exception e) {
            //
        } finally {
            bos.close();
        }
        return bos.toByteArray();
    }
}
