package com.citicbank;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.Key;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAKey;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import javax.crypto.Cipher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.alibaba.fastjson.JSONObject;
import com.lsy.baselib.crypto.util.Base64;
import com.lsy.baselib.crypto.util.FileUtil;

@WebServlet(urlPatterns={"/h5demo"})
public class H5demo extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		req.setCharacterEncoding("UTF-8");
		resp.setCharacterEncoding("UTF-8");
		String redirectURL = req.getParameter("redirectURL");
		HttpSession session = req.getSession();

		String paramsList = req.getParameter("paramsList");
		if (paramsList == null || "".equals(paramsList)) {
			session.setAttribute("errmsg", "参数列表为空");
		} else {
			session.setAttribute("errmsg", "");
		}

		String[] paramArr = paramsList.split(",");

		JSONObject json = new JSONObject();
		for (int i = 0; i < paramArr.length; i++) {
			String paraStr = paramArr[i];
			String value = req.getParameter(paraStr);
			json.put(paraStr, value);
		}

		String OPENTRANSCODE = req.getParameter("OPENTRANSCODE");
		json.put("OPENTRANSCODE", OPENTRANSCODE);
		String OPENMERCODE = req.getParameter("OPENMERCODE");
		json.put("OPENMERCODE", OPENMERCODE);
		String OPENBUSITYPE = req.getParameter("OPENBUSITYPE");
		json.put("OPENBUSITYPE", OPENBUSITYPE);
		String OPENMERNAME = req.getParameter("OPENMERNAME");
		json.put("OPENMERNAME", OPENMERNAME);
		json.put("OPENVER", "1.0.0");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		json.put("OPENLAUNCHDATE", sdf.format(new Date()));
		SimpleDateFormat sdf1 = new SimpleDateFormat("HHmmss");
		json.put("OPENLAUNCHTIME", sdf1.format(new Date()));
		SimpleDateFormat sdf2 = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		String uuid = UUID.randomUUID().toString().replace("-", "");
		json.put("OPENMERFLOWID", uuid + sdf2.format(new Date()));

		String jsonstr = json.toJSONString();
		System.out.println("*****************:" + jsonstr);
		String sign = signData(jsonstr);
		String encryptBody = null;
		try {
			encryptBody = encryptBusiness(jsonstr);
		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("***************************encrypt:"+encryptBody);
		System.out.println("***************************sign:"+sign);
		
		session.setAttribute("redirectURL", redirectURL);
		session.setAttribute("encryptBody",
				java.net.URLEncoder.encode(encryptBody, "UTF-8"));
		session.setAttribute("sign", java.net.URLEncoder.encode(sign, "UTF-8"));
		resp.sendRedirect("redirectUrl.jsp");
	}

	private String encryptBusiness(String business) throws Exception {
		String encryptKey = AESUtil.getRandomAESKey();
		byte[] encryptBusinessDataByte = AESUtil.encrypt(business, encryptKey);

		String encryptBusiness = new String(
				Base64.encode(encryptBusinessDataByte), "UTF-8");

		String publicKey = "MIIDQTCCAimgAwIBAgIBMDANBgkqhkiG9w0BAQUFADA6MQswCQYDVQQGEwJDTjENMAsGA1UECwwEQ05DQjEcMBoGA1UEAwwTYjJjLmJhbmsuZWNpdGljLmNvbTAgFw0xODEwMjIwMjU0MjZaGA8yMTE4MDkyODAyNTQyNlowOjELMAkGA1UEBhMCQ04xDTALBgNVBAsMBENOQ0IxHDAaBgNVBAMME2IyYy5iYW5rLmVjaXRpYy5jb20wggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCJe5k8J5oxkRbfrNHHbN8MEce9lkV/HWrKtpEtknRICpyT24Rx3xiCkyFfC2Zn0sYofJAcAvKElDupIIuYJHMvwWgakYHVHAbsp8XfGDToDeZdabCPSaV/tC+pbcZjnF+Zdlmy/TL9Yzxe/lSqhAnjk2wIeBQUy27CpcOKO7GXMVFE9TgogqDND+hYYzyaj+8gh73DRu2xSAq+ZTvJuW+bPvuALEOK/Ex8MO9u0oV1nn3OwkPkE/98jSs8BWBRDDGuh9OaZrUoNsF7II/e10Ad93lPf7UTEAiosO7ZiyJ+MBGy+cbkCwjpe5OOrkFr+9GNI6pjcDYul2KgI6XXdX0bAgMBAAGjUDBOMB0GA1UdDgQWBBTFTb+SBCIKxAdjuaOlferd/wKWYTAfBgNVHSMEGDAWgBTFTb+SBCIKxAdjuaOlferd/wKWYTAMBgNVHRMEBTADAQH/MA0GCSqGSIb3DQEBBQUAA4IBAQA4zYjdRonussCKJGAK+KKI1Ov8ptG9lxuUEjDFCYGG0hgyDfUxuNAIWI3GaDIxfreR/YMXPEOzEl4yORzFEu/ekh5gQvNT674x29BaA1iOKW1w5OJ7N1msJ76J66V4o1FQuFxZFO3U5nIx10eMe1VUezBIRrVO0YXMnCwA4Ckc9m1+5vEWhB78oLLSLgNhUcIwDyXyyb48TjeUO9TGCDECaadLLPsKSEiQ/fp5Nbo2VFrrx3IUMBJDxu6LiXAEZwQ8/cc8O//sT7t5ezu4joLNKGU8Sl6MSW5k2cee76g52MaA0Zt1x3G2viAMtkj5w1hJs8rsMA7+aty6tnbELWZe";
		byte[] base64EncodedSenderCert = publicKey.getBytes("UTF-8");

		X509Certificate signerCertificate = CryptUtil
				.generateX509Certificate(com.lsy.baselib.crypto.util.Base64
						.decode(base64EncodedSenderCert));
		PublicKey signpublicKey = signerCertificate.getPublicKey();

		byte[] encryptKeyByte = encrypt((RSAKey) signpublicKey,
				encryptKey.getBytes("UTF-8"));
		String encryptKeyString = new String(Base64.encode(encryptKeyByte),
				"UTF-8");

		encryptBusiness = encryptBusiness + "@@" + encryptKeyString;

		return encryptBusiness;
	}

	private byte[] encrypt(RSAKey key, byte[] data) {
		try {
			Cipher cipher = Cipher.getInstance("RSA/ECB/NoPadding", "BC");
			cipher.init(1, (Key) key);
			int step = key.getModulus().bitLength() / 8;
			int n = data.length / step;
			if (n > 0) {
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				for (int i = 0; i < n; i++) {
					baos.write(cipher.doFinal(data, i * step, step));
				}
				if ((n = data.length % step) != 0) {
					baos.write(cipher.doFinal(data, data.length - n, n));
				}
				return baos.toByteArray();
			}
			return cipher.doFinal(data);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private String signData(String data) {
		try {
			byte[] msg = data.getBytes("UTF-8");

			// 签名数据
			PrivateKey privateKey = getPrivateKey();
			Signature signature = Signature.getInstance("SHA1WithRSA");
			signature.initSign(privateKey);
			signature.update(msg);
			byte[] bytarr = signature.sign();

			String signData = new String(Base64.encode(bytarr), "UTF-8");
			signData = signData.replaceAll("\r|\n", "");
			return signData;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private PrivateKey getPrivateKey() throws Exception {
		// 私钥文件路径
		String keyfile = H5demo.class.getClassLoader()
				.getResource("signkey/server_sign.key").getPath();
		byte[] base64EncodedPrivatekey = FileUtil.read4file(keyfile);

		// 私钥密码文件路径
		String pwdfile = H5demo.class.getClassLoader()
				.getResource("signkey/server_sign.pwd").getPath();
		byte[] base64EncodedPrivatekeyPass = FileUtil.read4file(pwdfile);

		char[] keyPassword = new String(base64EncodedPrivatekeyPass, "UTF-8")
				.toCharArray();

		PrivateKey signerPrivatekey = CryptUtil.decryptPrivateKey( Base64.decode(base64EncodedPrivatekey), keyPassword);
		return signerPrivatekey;
	}
}
