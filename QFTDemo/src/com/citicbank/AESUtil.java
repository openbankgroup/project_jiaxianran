package com.citicbank;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class AESUtil {

	private static int KeySizeAES128 = 16;

	private static Cipher getCipher(int mode, String key) {
		// mode =Cipher.DECRYPT_MODE or Cipher.ENCRYPT_MODE
		Cipher mCipher;
		byte[] keyPtr = new byte[KeySizeAES128];
		IvParameterSpec ivParam = new IvParameterSpec(keyPtr);
		byte[] passPtr = key.getBytes();
		try {
			mCipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			for (int i = 0; i < KeySizeAES128; i++) {
				if (i < passPtr.length)
					keyPtr[i] = passPtr[i];
				else
					keyPtr[i] = 0;
			}
			SecretKeySpec keySpec = new SecretKeySpec(keyPtr, "AES");
			mCipher.init(mode, keySpec, ivParam);
			return mCipher;
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			e.printStackTrace();
		} catch (InvalidAlgorithmParameterException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static byte[] encrypt(String content, String password) {
		try {
			Cipher cipher = getCipher(Cipher.ENCRYPT_MODE, password);// 创建密码器
			byte[] result = cipher.doFinal(content.getBytes("UTF-8"));// 加密
			return result;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static byte[] decrypt(byte[] content, String password) {
		try {
			Cipher cipher = getCipher(Cipher.DECRYPT_MODE, password);// 创建密码器
			byte[] result = cipher.doFinal(content);
			return result; // 明文
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String getRandomAESKey() {
		int $aes_ken_len = 16;
		String aes_key_str = "";
		char[] e = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/="
				.toCharArray();
		int index = 0;
		Random r = new Random();
		for (int i = 0; i < $aes_ken_len; i++) {
			index = r.nextInt(64);
			aes_key_str += e[index];
		}
		return aes_key_str;
	}
}
